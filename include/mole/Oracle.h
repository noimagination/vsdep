#pragma once
#include <string>
using namespace std;

//#define ORACLE_USE_WIDE

#ifdef ORACLE_USE_WIDE
#define OCI_CHARSET_WIDE
typedef wstring orastring;
typedef wostringstream orastringstream;
#else
typedef string orastring;
typedef ostringstream orastringstream;
#endif

#include <ocilib/include/ocilib.h>

#ifdef ORACLE_EXEC_USE_LOG 
#include <mole/Loging.h>
#endif

struct HostParam
{
	orastring hostName;
	int		hostPort;
	orastring hostDbName;
	orastring hostUser;
	orastring hostPsw;
};

class COracle
{
public:
	COracle(void);
	virtual ~COracle(void);
	BOOL Connect(HostParam host);
	BOOL Connect(orastring hostName, int hostPort, orastring hostDbName,orastring hostUser, orastring hostPsw);
	BOOL DisConnect();
	void Attach(OCI_Connection* pCon);
	inline BOOL IsConnected(void){return OCI_IsConnected(m_pCon);}
	inline BOOL Ping(void){return OCI_Ping(m_pCon);}
	inline OCI_Connection* GetConPtr(void){return m_pCon;}
	inline HostParam GetHostParam(void){return m_hostParam;}
	inline orastring& GetLastErrMsg(void){return m_errMsg;}

	inline void Use(bool use){EnterCriticalSection(&m_cs);m_inuse=use;LeaveCriticalSection(&m_cs);}
	inline bool IfInuse(){EnterCriticalSection(&m_cs);bool ret = m_inuse; LeaveCriticalSection(&m_cs);return ret;}
protected:
	OCI_Connection*	m_pCon;
	HostParam m_hostParam;
	orastring m_errMsg;
	bool m_inuse;		// Support external link pool
	CRITICAL_SECTION m_cs;
public:
	static BOOL InitOCILib(void);
	static void FreeOCILib(void);
};


/**! 为简化代码封装一次 SQL 操作过程
 */
class CDBExecutor{
public:
	CDBExecutor(COracle* ora){
		m_ora = ora;
		m_stat = 0;
	}
	~CDBExecutor(){
		if(m_stat){
			OCI_StatementFree(m_stat);
			m_stat = 0;
		}
	}
public:
	// select
	OCI_Resultset* Query(orastring sql);
	// update delete insert
	bool Exec(orastring sql);
private:
	OCI_Statement* m_stat;
	COracle* m_ora;
	orastring m_temp;
};