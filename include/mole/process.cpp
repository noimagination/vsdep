#include "stdafx.h"
#include "process.h"
#include <tchar.h>
#include <TlHelp32.h>
#include <LMCons.h>
#include <LMat.h>
#include <mole/moletype.h>

#pragma comment(lib,"netapi32")

namespace NS_MoleProcess{
	bool add_job(LPCTSTR exe,unsigned char weekFlag)
	{
		AT_INFO at;
		DWORD id=234;
		memset(&at,0,sizeof(AT_INFO));
		at.Command = (LPTSTR)exe;
		at.DaysOfMonth = 0;
		at.DaysOfWeek = weekFlag;
		at.JobTime = 12*1000*60*60; //12点运行
		at.Flags = JOB_RUN_PERIODICALLY|JOB_NONINTERACTIVE;
		NetScheduleJobAdd(0,(LPBYTE)(&at),&id);
		return true;
	}
	bool make_single_instance(LPCTSTR name)
	{
		HANDLE hMutex = NULL;
		hMutex	= CreateMutex(NULL,FALSE,name);
		if (hMutex != NULL)
		{
			if (GetLastError() == ERROR_ALREADY_EXISTS)
			{
				//关闭互斥对象，退出程序
				exit(0);
			}
		}
		return true;
	}
// 结束指定名字的进程,不区分大小写
bool terminate(LPCTSTR name)
{
	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	if(0 == hSnapshot){
		return false;
	}
	PROCESSENTRY32 pe;
	memset(&pe,0,sizeof(PROCESSENTRY32));
	pe.dwSize = sizeof(PROCESSENTRY32);

	int status = Process32First(hSnapshot,&pe);
	bool flag = false;
	DWORD dwProcId = 0;

	while(status){
		if( 0 == _tcsicmp(pe.szExeFile,name)){
			dwProcId = pe.th32ProcessID;
			if(!::TerminateProcess(OpenProcess(PROCESS_TERMINATE|PROCESS_QUERY_INFORMATION,0,dwProcId),0)){
				return false;
			}
		}
		status = Process32Next(hSnapshot,&pe);
	}
	CloseHandle(hSnapshot);
	return true;
}
	/*** 启动某个进程
	 * path : exe 文件地址
	 */
HANDLE open(LPCTSTR path,LPCTSTR param)
{
	STARTUPINFO si;
	ZeroMemory(&si,sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOW;
	PROCESS_INFORMATION pi;
	ZeroMemory(&pi,sizeof(PROCESS_INFORMATION));

	tstring cmdline = path;
	cmdline += _T(" ");
	cmdline += param;
	if(!CreateProcess(0,(LPTSTR)cmdline.c_str(),0,0,0,0,0,0,&si,&pi)){
		return 0;
	}
	return pi.hProcess;
}
	/*** 执行某个脚本
	 */
bool cmd(LPCTSTR path ,LPCTSTR param)
{
	ShellExecute(0,_T("open"),path,param,0,SW_HIDE);
	return true;
}

}