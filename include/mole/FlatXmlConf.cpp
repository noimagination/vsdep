#include "StdAfx.h"
#include "FlatXmlConf.h"

#ifdef SLIM_USE_WCHAR
#define GetModuleFileNameT GetModuleFileNameW
#else
#define GetModuleFileNameT GetModuleFileNameA
#endif

String same_name_conf(HMODULE h,String ext)
{
	Char szFile[MAX_PATH*2];
	String ret;
	GetModuleFileNameT(h,szFile,MAX_PATH);
	ret = szFile;
	ret = ret.substr(0,ret.find_last_of(T(".")));
	ret += ext;
	return ret;
}
int auto_load_config(HMODULE h,CFlatXmlConf* conf)
{
	if(!conf){
		return ME_PARAM_PTR_NULL;
	}
	String xml = same_name_conf(h);
	return conf->load(xml.c_str());
}
CFlatXmlConf::CFlatXmlConf(void)
{
}

CFlatXmlConf::~CFlatXmlConf(void)
{
}
int CFlatXmlConf::load(const Char* xml_file)
{
	xDoc xml;
	if(!xml.loadFromFile(xml_file)){
		return ME_FILE_OPEN;
	}
	xNode root;
	slim::NodeIterator it;
	root = xml.getFirstChild(it);
	while(root){
		m_conf[root->getName()]=root->getString();

		root = xml.getNextChild(it);
	}
	return ME_OK;
}
String CFlatXmlConf::get(const String& name)
{
	if(!this->has(name)){
		return String(T(""));
	}
	return m_conf[name];
}
bool CFlatXmlConf::has(const String& name)
{
	return m_conf.end() != m_conf.find(name);
}
int CFlatXmlConf::set(const String&name,const String&value)
{
	m_conf[name]=value;
	return ME_OK;
}