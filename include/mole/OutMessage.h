#pragma once

#include <string>
#include <mole/Lock.h>
#include <mole/molerror.h>

#ifdef UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif

#define MAX_ERROR 512

typedef enum OutMessageType
{
	OMT_Status,
	OMT_Debug,
	OMT_Error,
	OMT_WARNING
}MsgType;

struct OutMessage
{
	MsgType msgType;
	TCHAR msgTime[24];
	TCHAR text[MAX_ERROR];
	int code;
	unsigned long ptr;

	OutMessage(MsgType _msgType, LPCTSTR s,int errcode=MOLE_ERROR_CODE_DEF,unsigned long p=0)
	{
		msgType = _msgType;
		code = errcode;
		ptr = p;

		SYSTEMTIME st;
		GetLocalTime(&st);

		int nWrite = _stprintf_s(msgTime, 32,_T("%04d/%02d/%02d %02d:%02d:%02d:%03d"), 
			st.wYear,st.wMonth, st.wDay, st.wHour, 
			st.wMinute, st.wSecond, st.wMilliseconds);

		if(s){
			_tcscpy_s(text,s);
		}else{
			text[0]=0;
		}
	}
};

typedef std::list<OutMessage*> OutMessagePool;
typedef OutMessagePool::iterator DebugStringIter;

#define myTrace(str) COutMessageHandler::Get()->Trace(str)
#define myTrace1(str,a) COutMessageHandler::Get()->Trace(str,a)
#define myTrace2(str,a,b) COutMessageHandler::Get()->Trace(str,a,b)
#define myTrace3(str,a,b,c) COutMessageHandler::Get()->Trace(str,a,b,c)
#define myTrace4(str,a,b,c,d) COutMessageHandler::Get()->Trace(str,a,b,c,d)

#define myStatus(str) COutMessageHandler::Get()->Status(str)
#define myStatus1(str,a) COutMessageHandler::Get()->Status(str,a)
#define myStatus2(str,a,b) COutMessageHandler::Get()->Status(str,a,b)
#define myStatus3(str,a,b,c) COutMessageHandler::Get()->Status(str,a,b,c)
#define myStatus4(str,a,b,c,d) COutMessageHandler::Get()->Status(str,a,b,c,d)

#define myError(str) COutMessageHandler::Get()->Error(str)
#define myError1(str,a) COutMessageHandler::Get()->Error(str,a)
#define myError2(str,a,b) COutMessageHandler::Get()->Error(str,a,b)
#define myError3(str,a,b,c) COutMessageHandler::Get()->Error(str,a,b,c)
#define myError4(str,a,b,c,d) COutMessageHandler::Get()->Error(str,a,b,c,d)

#define OUTMSG COutMessageHandler::Get()
class COutMessageHandler
{
private:
	COutMessageHandler(void);
	~COutMessageHandler(void);
	enum {mt_pool=0,mt_count};
private:
	OutMessagePool m_messagePool;
	CCriticalLock	m_mtObj[mt_count];
	static CCriticalLock m_mtInstanse;
public:
	void Trace(LPCTSTR str,...);
	void Status(LPCTSTR str,...);
	void Error(LPCTSTR str,...);

	void PushMessage(MsgType mt,unsigned long pointer,int code,LPCTSTR str,...);
	OutMessage* PickMessage();
public:
	static void Init();
	static COutMessageHandler* Get();
	static void UnInit();
private:
	void _PushMessage(MsgType mt,unsigned long pointer,int code,LPCTSTR str,va_list p);
	static COutMessageHandler* pHandler;
};