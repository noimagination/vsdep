#ifndef MOLE_PROCESS_20141123
#define MOLE_PROCESS_20141123

#include <windows.h>

namespace NS_MoleProcess{
	/***
	添加计划任务，每周几执行

	 */
	bool add_job(LPCTSTR exe,unsigned char weekFlag);
	bool make_single_instance(LPCTSTR name);
	// 结束指定名字的进程,不区分大小写
	bool terminate(LPCTSTR name);
	/*** 启动某个进程
	 * path : exe 文件地址
	 */
	HANDLE open(LPCTSTR path,LPCTSTR param);
	/*** 执行某个脚本
	 */
	bool cmd(LPCTSTR path ,LPCTSTR param);
};

#endif
