//#include "stdafx.h"
#include "httpClient.h"
#include <stdio.h>
#include <tchar.h>

#pragma comment(lib,"wininet.lib")

CHttpClient::CHttpClient()
{
	m_hConnection = 0;
	reset();

	m_hostName[0]=0;
	m_urlPath[0]=0;
}
HINTERNET CHttpClient::m_hInternet = 0;

CHttpClient::~CHttpClient()
{
	this->disconnect();
}

bool CHttpClient::init()
{
	m_hInternet = InternetOpen(_T("MoleUpdater"),INTERNET_OPEN_TYPE_PRECONFIG,0,0,0);
	return m_hInternet != 0;
}
bool CHttpClient::destroy()
{
	if(m_hInternet){
		InternetCloseHandle(m_hInternet);
		m_hInternet = 0;
	}
	return true;
}
bool CHttpClient::connect(LPCTSTR url)
{
	if(!InternetCrackUrl(url,(DWORD)_tcslen(url),0,&m_url)){
		DWORD err = GetLastError();
		return false;
	}
	m_hConnection = InternetConnect(m_hInternet,
		m_url.lpszHostName,INTERNET_DEFAULT_HTTP_PORT,0,0,INTERNET_SERVICE_HTTP,INTERNET_FLAG_PASSIVE,0);
	return m_hConnection != NULL;
}
void CHttpClient::reset()
{
	memset(&m_url,0,sizeof(URL_COMPONENTS));
	m_url.dwStructSize = sizeof(URL_COMPONENTS);
	m_url.lpszHostName = m_hostName;
	m_url.dwHostNameLength = LEN_HOSTNAME;
	m_url.lpszUrlPath = m_urlPath;
	m_url.dwUrlPathLength = LEN_URLPATH;
}
bool CHttpClient::disconnect()
{
	if(m_hConnection){
		InternetCloseHandle(m_hConnection);
		m_hConnection = 0;
	}
	return true;
}
bool CHttpClient::download_file(LPCTSTR url,LPCTSTR save_to)
{
	FILE* fp = 0;
	HINTERNET hFile = 0;
	char buf[1024]={0};
	DWORD buf_len = 1024;
	DWORD read = 0;
	DWORD status = 0;
	DWORD dwSizeofRq = 4;
	BOOL query= FALSE;
	
	_tfopen_s(&fp,save_to,_T("wb"));
	if(!fp){
		return false;
	}
	reset();
	if(!this->connect(url)){
		fclose(fp);
		return false;
	}
	hFile = HttpOpenRequest(m_hConnection,_T("GET"),
		m_url.lpszUrlPath,HTTP_VERSION,0,0,INTERNET_FLAG_NO_UI|INTERNET_FLAG_DONT_CACHE,0);
	if(!hFile){
		fclose(fp);
		this->disconnect();
		return false;
	}

	if(!HttpSendRequest(hFile,0,0,0,0)){
		fclose(fp);
		this->disconnect();
		InternetCloseHandle(hFile);
		return false;
	}
	query = HttpQueryInfo(hFile,HTTP_QUERY_STATUS_CODE|HTTP_QUERY_FLAG_NUMBER,(LPVOID)&status,
		&dwSizeofRq,NULL);
	if(!query || status != 200){
		InternetCloseHandle(hFile);
		this->disconnect();
		fclose(fp);

		return false;
	}
	while(1){
		InternetReadFile(hFile,buf,buf_len,&read);
		if(0 == read){
			break;
		}
		fwrite(buf,sizeof(char),read,fp);
		Sleep(0);
	}

	InternetCloseHandle(hFile);
	this->disconnect();
	fclose(fp);

	return true;
}

bool CHttpClient::download_mem(LPCTSTR url,char* buffer)
{
	HINTERNET hFile = 0;
	DWORD read = 0;
	DWORD status = 0;
	DWORD dwSizeofRq = 4;
	BOOL query= FALSE;

	reset();
	if(!this->connect(url)){
		return false;
	}
	hFile = HttpOpenRequest(m_hConnection,_T("GET"),
		m_url.lpszUrlPath,HTTP_VERSION,0,0,INTERNET_FLAG_NO_UI|INTERNET_FLAG_DONT_CACHE,0);
	if(!hFile){
		this->disconnect();
		return false;
	}

	if(!HttpSendRequest(hFile,0,0,0,0)){
		this->disconnect();
		InternetCloseHandle(hFile);
		return false;
	}
	query = HttpQueryInfo(hFile,HTTP_QUERY_STATUS_CODE|HTTP_QUERY_FLAG_NUMBER,(LPVOID)&status,
		&dwSizeofRq,NULL);
	if(!query || status != 200){
		InternetCloseHandle(hFile);
		this->disconnect();

		return false;
	}
	int every=1024;
	int total=0;
	while(1){
		InternetReadFile(hFile,buffer+total,every,&read);
		if(0 == read){
			break;
		}
		total+=read;
		Sleep(0);
	}
	buffer[total] = 0;

	InternetCloseHandle(hFile);
	this->disconnect();

	return true;
}
bool CHttpClient::post(LPCTSTR url,char* data)
{
	return false;
}