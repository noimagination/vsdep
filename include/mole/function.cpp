#include "stdafx.h"
#include "function.h"
#include <windows.h>
#include <jwSMTP/mailer.h>
#include <charset_convert.h>
#include <zip/unzip.h>
#include <atlconv.h>
#include <time.h>
#include <fstream>

#ifdef _DEBUG
#pragma comment(lib,"jwSMTPd.lib")
//#pragma comment(lib,"ziputilssud.lib")
#else
#pragma comment(lib,"jwSMTP.lib")
//#pragma comment(lib,"ziputilssu.lib")
#endif

wstring g_runDirW;
string g_runDirA;

time_t time2date(time_t t)
{
	struct tm t1,ret;
	localtime_s(&t1,&t);

	memset(&ret,0,sizeof(struct tm));
	ret.tm_year = t1.tm_year;
	ret.tm_mon = t1.tm_mon;
	ret.tm_mday = t1.tm_mday;

	return mktime(&ret);
}
void InitRunDir(tstring& rundir)
{
	wchar_t tcWorkPath[MAX_PATH] = {0};
	char c[MAX_PATH]={0};

	GetModuleFileNameW(nullptr,tcWorkPath,MAX_PATH);
	GetModuleFileNameA(nullptr,c,MAX_PATH);
	g_runDirW = tcWorkPath;
	g_runDirA = c;
	g_runDirW = g_runDirW.substr(0,g_runDirW.find_last_of(_T('\\')) +1);
	g_runDirA = g_runDirA.substr(0,g_runDirW.find_last_of(_T('\\')) +1);
	rundir = g_runDirW;
}

int mail_send(tstring title,tstring content,tstring dest)
{
	USES_CONVERSION;
	string title_=W2A(title.c_str());
	string content_=W2A(content.c_str());
	string dest_=W2A(dest.c_str());

	jwsmtp::mailer m(dest_.c_str(), "yyk882002@163.com", title_.c_str(),
		"", "smtp.163.com",
		jwsmtp::mailer::SMTP_PORT, false);
	m.authtype(jwsmtp::mailer::PLAIN);
	std::string c = s2utfs(content_.c_str());
	m.setmessageHTML(c);

	m.username("yyk882002@163.com");
	m.password("initialize");
	m.send(); // send the mail
	string ret = m.response();
	return (ret.find("250") == string::npos)?-1:0;
}
/*
bool zip_unzip(LPCTSTR zip_file,LPCTSTR dest_dir)
{
	HZIP hz = OpenZip(zip_file,0);
	ZIPENTRY ze; 
	GetZipItem(hz,-1,&ze); 
	int numitems=ze.index;
	TCHAR dest_file[1024];
	// -1 gives overall information about the zipfile
	for (int zi=0; zi<numitems; zi++){ 
		ZIPENTRY ze; 
		GetZipItem(hz,zi,&ze); // fetch individual details

		_tcscpy_s(dest_file,dest_dir);
		_tcscat_s(dest_file,_T("\\"));
		_tcscat_s(dest_file,ze.name);
		UnzipItem(hz, zi, dest_file);         // e.g. the item's name.
	}
	CloseZip(hz);
	return true;
}
*/
bool save_file(const TCHAR* file,const TCHAR* content)
{
	USES_CONVERSION;
	ofstream of;
	char* f = T2A(file);
	char* c = T2A(content);
	of.open(f);
	if(!of.is_open()){
		return false;
	}
	int len = strlen(c);

	
	of.write(c,len);
	of.close();

	return true;
}
tstring correct_filename(const tstring&filename)
{

	tstring pattern = _T("[\\/:*\"<>|\?]");
	tstring ret = filename;

	tstring::size_type n= 0;
	while(1){
		n = ret.find_first_of(pattern.c_str());
		if(n != tstring::npos){
			ret[n]=_T('y');
		}else{
			break;
		}
	}
	return ret;
}
