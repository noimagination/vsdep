#include "StdAfx.h"
#include "Loging.h"
#include <windows.h>
#include <time.h>

CLoging::CLoging(void)
{
	m_logfile = _T("");
	m_bAlive = true;
	m_fp = 0;
}


CLoging::~CLoging(void)
{
	if(m_fp){
		fclose(m_fp);
		m_fp = 0;
	}
}

void CLoging::SetAlive(bool val)
{
	m_bAlive = val;
}
void CLoging::SetLogFile(const LCHAR* fullpath)
{
	if(!fullpath || _tcslen(fullpath) == 0){
		TCHAR dir[MAX_PATH];
		TCHAR name[MAX_PATH];
		time_t now;struct tm n;
		GetCurrentDirectory(MAX_PATH,dir);

		time(&now);
		localtime_s(&n,&now);
		_stprintf_s(name,MAX_PATH,_T("%04d-%02d-%02d %02d-%02d-%02d.log"),
			n.tm_year+1900,n.tm_mon+1,n.tm_mday,n.tm_hour,n.tm_min,n.tm_sec);

		_tcscat_s(dir,_T("\\"));
		_tcscat_s(dir,name);

		m_logfile = dir;
	}else{
		m_logfile = fullpath;
	}
	WORD wSig = 0xFEFF; // UNICODE文件头
	
	_tfopen_s(&m_fp,fullpath,_T("w"));
	if(m_fp){
		fwrite(&wSig,sizeof(char),sizeof(WORD),m_fp);
		fclose(m_fp);
	}
	
	m_fp = 0;
}

/*
void CLoging::SetLogFile2(const char* fullpath)
{
	if(!fullpath || _tcslen(fullpath) == 0){
		char dir[MAX_PATH];
		char name[MAX_PATH];
		time_t now;struct tm n;
		GetCurrentDirectory(MAX_PATH,dir);

		time(&now);
		localtime_s(&n,&now);
		sprintf_s(name,MAX_PATH,_T("%04d-%02d-%02d %02d-%02d-%02d.log"),
			n.tm_year+1900,n.tm_mon+1,n.tm_mday,n.tm_hour,n.tm_min,n.tm_sec);

		strcat_s(dir,_T("\\"));
		strcat_s(dir,name);

		m_logfile = dir;
	}else{
		m_logfile = fullpath;
	}
	WORD wSig = 0xFEFF; // UNICODE文件头

	fopen_s(&m_fp,fullpath,("w"));
	if(m_fp){
		fclose(m_fp);
	}

	m_fp = 0;
}

void CLoging::Log2(const char* content, bool wt)
{
	fopen_s(&m_fp,m_logfile.c_str(),"a+");
	if(!m_fp){
		return;
	}
	char real_c[64];
	time_t now;
	time(&now);
	struct tm t;
	localtime_s(&t,&now);

	sprintf_s(real_c,64,("[%04d-%02d-%02d %02d:%02d:%02d]："),
		t.tm_year+1900,t.tm_mon+1,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec);

	ostringstream oss;
	oss<<real_c<<content<<std::endl;

	fwrite(oss.str().c_str(),sizeof(char),oss.str().length(),m_fp);

	fflush(m_fp);
	fclose(m_fp);
	m_fp = 0;
}*/
void CLoging::DLog2(const char* c,bool wt)
{

}
void CLoging::Log(const TCHAR* content,bool with_time)
{
	TCHAR *mode=_T("a+,ccs=UNICODE");
	_tfopen_s(&m_fp,m_logfile.c_str(),mode);
	if(!m_fp){
		return;
	}
	TCHAR real_c[64];
	time_t now;
	time(&now);
	struct tm t;
	localtime_s(&t,&now);

	_stprintf_s(real_c,64,_T("[%04d-%02d-%02d %02d:%02d:%02d]："),
		t.tm_year+1900,t.tm_mon+1,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec);

	tostringstream oss;
	oss<<real_c<<content<<std::endl;

	fwrite(oss.str().c_str(),sizeof(TCHAR),oss.str().length(),m_fp);
	/*
	fwrite(real_c,sizeof(TCHAR),_tcslen(real_c),m_fp);
	fwrite(content,sizeof(TCHAR),_tcslen(content),m_fp);
	fwrite(_T("\n"),sizeof(TCHAR),_tcslen(_T("\n")),m_fp);
	*/
	fflush(m_fp);
	fclose(m_fp);
	m_fp = 0;
}
void CLoging::DLog(const LCHAR* c,bool wt)
{
#ifdef DEBUG
	Log(c,wt);
#endif
}

//===========