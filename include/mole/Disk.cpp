#include "StdAfx.h"
#include "Disk.h"
#include <ShellAPI.h>
#include <windows.h>
#include <ShlObj.h>

#pragma comment(lib,"shell32.lib")

tstring CPartition::ms_current_dir=_T("");
tstring CPartition::ms_me=_T("");

CPartition::CPartition(const TCHAR* fromPath)
{
	m_partition = fromPath;
	m_partition = m_partition.substr(0,2);
	this->StatisticPartition();
}
CPartition::~CPartition()
{
}

BOOL CPartition::IsFileExists(const TCHAR * file)
{
	return INVALID_FILE_ATTRIBUTES != GetFileAttributes(file);
}
bool CPartition::CreateDir(LPCTSTR dir)
{
	return ::SHCreateDirectoryEx(NULL,dir,NULL);
}
 bool CPartition::IsFileExistsEx(LPCTSTR file)
 {
	 tstring f = file;
	 f +=_T("*.*");
	 WIN32_FIND_DATA fd;
	 HANDLE hFind = FindFirstFile(f.c_str(),&fd);
	 FindClose(hFind);
	 return hFind != INVALID_HANDLE_VALUE;
 }
BOOL CPartition::DeleteDirectory(LPCTSTR dir)
{
	SHFILEOPSTRUCT FileOp;

	FileOp.fFlags = FOF_NOCONFIRMATION; 
	FileOp.hNameMappings = NULL; 
	FileOp.hwnd = NULL; 
	FileOp.lpszProgressTitle = NULL; 
	FileOp.pFrom = dir; 
	FileOp.pTo = NULL; 
	FileOp.wFunc = FO_DELETE; 

	int ok = SHFileOperation(&FileOp); 
	return 0==ok;
}
int CPartition::Search(LPCTSTR cond,IFileSearchCallback* cb,DWORD dwUser)
{
	WIN32_FIND_DATA fd;
	HANDLE hFind = FindFirstFile(cond,&fd);
	if(!hFind){
		return -1;
	}
	tstring path = cond;
	path = path.substr(0,path.find_last_of(_T("/\\"))+1);

	while(1){
		if(fd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY){
			if(cb){
				cb->OnFound(true,(path+fd.cFileName+_T('\0')).c_str(),dwUser);
			}
		}else if(fd.dwFileAttributes&FILE_ATTRIBUTE_NORMAL){
			if(cb){
				cb->OnFound(false,(path+fd.cFileName).c_str(),dwUser);
			}
		}
		if(!FindNextFile(hFind,&fd)){
			break;
		}
	}
	FindClose(hFind);
	return 0;
}//�Ǹ��绰03916553605
tstring CPartition::GetMe()
{
	if(ms_me.empty()){
		TCHAR dir[MAX_PATH]={0};
		GetModuleFileName(0,dir,MAX_PATH);
		ms_me = dir;
	}
	return ms_me;
}
tstring CPartition::GetThisPath()
{
	if(ms_current_dir.empty()){
		TCHAR dir[MAX_PATH]={0};
		GetModuleFileName(0,dir,MAX_PATH);
		ms_current_dir = dir;
		ms_current_dir = ms_current_dir.substr(0,ms_current_dir.find_last_of(_T("\\")));
	}

	return ms_current_dir;
}
void CPartition::ClearEmptyFolder(const TCHAR* root)
{
	tostringstream cmd[3];

	cmd[0]<<_T("dir ")<<root<<_T(" /ad /b /s |sort /r >>")<<root<<_T("\\kill.txt");
	
	cmd[1]<<_T("For /f %%i in (")<<root<<_T("\\kill.txt) DO rd %%i");
	cmd[2]<<_T("del ")<<root<<_T("\\kill.txt");
	tsystem(cmd[0].str().c_str());
	tsystem(cmd[1].str().c_str());
	tsystem(cmd[2].str().c_str());
}
P_SIZE CPartition::GetTotal(PARTITION_SIZE_UNIT psu)
{
	return m_maxSize/psu;
}
P_SIZE CPartition::GetFree(PARTITION_SIZE_UNIT psu)
{
	return m_freeSize/psu;
}
tstring CPartition::GetPartitionName()
{
	return m_partition;
}
void CPartition::StatisticPartition()
{
	ULARGE_INTEGER freeToCaller,total,free;
	if(GetDiskFreeSpaceEx(m_partition.c_str(),&freeToCaller,&total,&free)){
		m_maxSize = total.QuadPart;
		m_freeSize = freeToCaller.QuadPart;
	}else{
		m_maxSize = 0;
		m_freeSize = 0;
	}
}

//==========================================================
CDisk::CDisk(void)
{
}


CDisk::~CDisk(void)
{
}
