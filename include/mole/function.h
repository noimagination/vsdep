#pragma once
#include <mole/moletype.h>
#include <windows.h>

#define YESTERDAY(time__t)((time__t) - 60*60*24)
#define TOMORROW(time__t) ((time__t) + 60*60*24)

/**! 把时间规整为日期

 比如一个 time_t 代表 2014-12-31 08:59:42
 可转化为 2014-12-31 00:00:00
*/
time_t time2date(time_t t);
void InitRunDir(tstring& rundir);
int mail_send(tstring title,tstring content,tstring dest=_T("yyk882002@126.com"));
//bool zip_unzip(LPCTSTR zip,LPCTSTR dest_dir);

bool save_file(const TCHAR* file,const TCHAR* content);
/**! 纠正有些非法的文件名 */
tstring correct_filename(const tstring& filename);
