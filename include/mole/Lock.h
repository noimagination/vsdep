#ifndef MOLE_LOCK_H
#define  MOLE_LOCK_H

#include <winbase.h>

class ILockable{
public:
	virtual bool Lock()=0;
	virtual bool Unlock()=0;
};

class CCriticalLock:public ILockable{
	CRITICAL_SECTION m_cs;
public:
	CCriticalLock(){
		InitializeCriticalSection(&m_cs);
	}
	~CCriticalLock(){
		DeleteCriticalSection(&m_cs);
	}
	virtual bool Lock(){
		EnterCriticalSection(&m_cs);
		return true;
	}
	virtual bool Unlock(){
		LeaveCriticalSection(&m_cs);
		return true;
	}
};
//template<typename T>
class CAutoLock{
	ILockable* m_lock;
public:
	explicit CAutoLock(ILockable* lock){
		m_lock = lock;
		m_lock->Lock();
	}
	~CAutoLock(){
		m_lock->Unlock();
	}
};

class CWinEvent{
public:
	CWinEvent(){

	}
	~CWinEvent(){

	}

	bool SetEvent(){
		return true;
	}
private:
	HANDLE m_hEvent;
};
#endif
