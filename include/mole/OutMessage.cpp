#include "StdAfx.h"
#include "outmessage.h"

COutMessageHandler* COutMessageHandler::pHandler = nullptr;
CCriticalLock COutMessageHandler::m_mtInstanse;
COutMessageHandler::COutMessageHandler(void)
{
}


COutMessageHandler::~COutMessageHandler(void)
{
	m_messagePool.clear();
}

void COutMessageHandler::Trace( LPCTSTR str,... )
{
	va_list arg_ptr;
	va_start(arg_ptr, str);

	_PushMessage(OMT_Debug,0,MOLE_ERROR_CODE_DEF,str,arg_ptr);
}

OutMessage* COutMessageHandler::PickMessage()
{
	CAutoLock lock(&m_mtObj[mt_pool]);
	OutMessage *om=nullptr;//(OMT_Status,_T(""));

	if (m_messagePool.size()>0)
	{
		om = m_messagePool.front();
		m_messagePool.pop_front();
	}
	return om;
}

void COutMessageHandler::Init()
{
	pHandler = new COutMessageHandler;
}

COutMessageHandler* COutMessageHandler::Get()
{
	CAutoLock lock(&m_mtInstanse);
	return pHandler;
}

void COutMessageHandler::Status( LPCTSTR str,... )
{
	va_list arg_ptr;
	va_start(arg_ptr, str);

	_PushMessage(OMT_Status,0,MOLE_ERROR_CODE_DEF,str,arg_ptr);
	va_end(arg_ptr);
}

void COutMessageHandler::Error( LPCTSTR str,... )
{
	va_list arg_ptr;
	va_start(arg_ptr, str);

	_PushMessage(OMT_Error,0,MOLE_ERROR_CODE_DEF,str,arg_ptr);
	va_end(arg_ptr);
}
void COutMessageHandler::PushMessage(MsgType mt,unsigned long pointer,int code,LPCTSTR str,...)
{
	va_list arg_ptr;
	va_start(arg_ptr, str);

	_PushMessage(mt,pointer,code,str,arg_ptr);
	va_end(arg_ptr);
}
void COutMessageHandler::_PushMessage(MsgType mt,unsigned long pointer,int code,LPCTSTR str,va_list p)
{
	TCHAR szString[MAX_ERROR] = {0};
	_vstprintf(szString,MAX_ERROR,str, p);

	OutMessage *msg=new OutMessage(mt,szString,code,pointer);
	CAutoLock lock(&m_mtObj[mt_pool]);
	m_messagePool.push_back(msg);
}
void COutMessageHandler::UnInit()
{
	delete pHandler;
}

