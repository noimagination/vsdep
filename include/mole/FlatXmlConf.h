/********************************************************************
	created:	2015/08/24
	created:	24:8:2015   14:33
	filename: 	D:\vsdep\include\mole\FlatXmlConf.h
	file path:	D:\vsdep\include\mole
	file base:	FlatXmlConf
	file ext:	h
	author:		yyk
	
	purpose:	解析 Xml ，一般用于配置。
				这个 Xml 文件必须是平面的。不能是树形的。否则子树不会被读取
				配置全部以 string 类型保存。

				在应用中按需要转换为“逻辑正确”的类型。
	dependency: #pragma comment(lib,"slimxml_xx.lib")

	example xml file content : 

	<?xml version="1.0" encoding="UTF-8"?>
	<saveto>d:\image</saveto>
	<mgc>1</mgc>
	<what>nothing</what>
*********************************************************************/
#ifndef MOLE_FLAT_XML_CONF_H
#define MOLE_FLAT_XML_CONF_H

#include <Slimxml/xmlinc.h>
#include <mole/molerror.h>
#include <mole/moletype.h>

using namespace slim;
class CFlatXmlConf;
/**> 得到和*程序*同名的 xml 配置文件
 *
	@h : 句柄，可以是一个dll,也可以是一个 exe
	@return: xml 全路径
 *
	主要用于 dll 插件配置的自动加载。
 */
extern "C" String same_name_conf(HMODULE h,String ext=T(".xml"));


/**> @brief 自动加载 h 可执行文件的配置文件
 *
 * @h: dll 句柄
 * @conf : 配置
 * @return : ME_OK表示成功。
 * 
 * conf 作为返回值使用，内部是一个 stl map 结构，
 * 各个配置以 stl string 的名值对呈现。
 */
extern "C" int auto_load_config(HMODULE h,CFlatXmlConf* conf);
/**>
 */
class __declspec(dllexport) CFlatXmlConf
{
public:
	CFlatXmlConf(void);
	~CFlatXmlConf(void);
public:
	int load(const Char* xml_file);
	String get(const String& name);
	bool has(const String& name);
	/** 设置某个项，当不存在则添加 */
	int set(const String&name,const String&value);
private:
	map<String,String> m_conf;
};

#endif