/**! 降級的 vector
   *
   * 一個更通用的 vector，可以管理任何結構體
   * 優點：
   *			1,不必再爲每一種數據結構製作一個 vector 管理類
   *			2,支持多線程同步
   *		
   * 缺點：
   *			1,一定程度上失去了 STL 的靈活性
   *         
   * Contact:  yyk882002@gmail.com
   */
#ifndef DEGRADE_VECTOR_H
#define DEGRADE_VECTOR_H

#include <WinBase.h>
#include <list>
#include <vector>
#include <algorithm>

// Windows 平臺 互斥體
class Locker{
public:
	Locker(){InitializeCriticalSection(&cs);}
	virtual ~Locker(){DeleteCriticalSection(&cs);}
public:
	// 結對操作 // 
	void Lock(){EnterCriticalSection(&cs);}
	void Unlock(){LeaveCriticalSection(&cs);}
private:
	CRITICAL_SECTION cs;
};

// 自動鎖 
// 使用方式：函數內部臨時變量
class AutoLocker{
public:
	AutoLocker(Locker& locker):m_locker(locker){m_locker.Lock();}
	virtual ~AutoLocker(){m_locker.Unlock();}
private:
	Locker& m_locker;
};
/**! 結構體接口
   *
   * 所有被管理的結構體都必須實現此接口
   * 成員函數 get_key() 返回結構體的關鍵字，任意類型（一般指內置類型）
   */
template<typename Key>
class findable{
public: 
	virtual ~findable(){}
	virtual Key& get_key()=0;				// 返回本類型的關鍵字段
	virtual  char* get_name(){return "";};	// 返回一個字符串作爲標識
	char* get_data(){return (char*)this+sizeof(void*);}	// 得到內部數據首地址，用於 memcpy()
	bool operator < (const findable<Key>& o2){
		return this->get_key() < o2.get_key();
	}
};
// 返回非法索引，輔助函數
template <typename T>
T& int_invalid_key(){
	static T a=(T)0;
	return a;
}

//  重載小於號，用於排序等操作
template <typename Key>
struct __less{
	bool operator()(findable<Key>*a,findable<Key>*b){
		if(!a || !b)return false;
		return a->get_key() < b->get_key();
	}
};
/**! 通用管理類，示例代碼在 Test 工程中查找
   */
template<typename Key,template<class T,typename=std::allocator<T>> class C>
class devector{
	typedef typename C< findable< Key >* > SET;
	typedef typename C< findable< Key >* >::iterator ITOR;
public:
	devector(){this->clear();}
	virtual ~devector(){}
public:
	findable<Key>* operator[](int index){
		if(index < 0 || index >= (int)m_set.size())
			return 0;
		AutoLocker al(m_locker);
		ITOR it = m_set.begin();
		int i = 0;
		for(; i < index; i++)
			it++;
		return *it;
	}
	void push_back(findable<Key>* item){
		AutoLocker al(m_locker);
		if(item){
			m_set.push_back(item);
		}
	}
	findable<Key>* find(Key k){
		AutoLocker al(m_locker);
		ITOR it;
		for(it = m_set.begin(); it != m_set.end(); it++){
			if((*it)->get_key() == k){
				int a = (int)(*it);
				return *it;
			}
		}
		return 0;
	}
	bool erase(Key k){
		AutoLocker al(m_locker);
		ITOR it;
		for(it = m_set.begin(); it != m_set.end(); it++){
			if((*it)->get_key() == k){
				int a = (int)(*it);
				delete (findable<Key>*)(*it);
				(*it) = 0;
				m_set.erase(it);
				return true;
			}
		}
		return false;
	}
	void clear(){
		AutoLocker al(m_locker);
		ITOR it;
		for(it = m_set.begin(); it != m_set.end(); it++){
			if(*it){
				delete (*it);
				*it = 0;
			}
		}
		m_set.clear();
	}
	std::size_t size(){
		AutoLocker al(m_locker);
		return m_set.size();
	}
	void sort(){
		AutoLocker al(m_locker);
		__less<Key> l;
		std::sort(m_set.begin(),m_set.end(),l);
	}
	void sortl(){
		AutoLocker al(m_locker);
		m_set.sort(__less<Key>());
	}
	bool empty(){
		AutoLocker al(m_locker);
		return m_set.empty();
	}
protected:
	ITOR m_it;
	SET m_set;
	Locker m_locker;
};
// 定义辅助
typedef devector<__int64,std::vector> GV;
typedef devector<__int64,std::list> GL;
#endif