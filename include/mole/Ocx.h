#pragma once
#include "moletype.h"

class COcx
{
public:
	COcx(LPCTSTR ocxFile,LPCTSTR class_name/* FACESERVERSDK.FaceServerSdkCtrl.1*/);
	~COcx();
	
	bool IsRegistered();
	bool Register();
	bool Unregister();
private:
	tstring m_file;
	tstring m_name;
};

