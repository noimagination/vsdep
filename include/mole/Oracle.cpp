#include "stdafx.h"
//#include <mole/moletype.h>
#include <mole/Oracle.h>
//#include <atlconv.h>
//#include <tchar.h>

#pragma comment(lib,"ocilibw.lib")

COracle::COracle(void)
{
	m_pCon = NULL;
	m_inuse=false;
	InitializeCriticalSection(&m_cs);
}

COracle::~COracle(void)
{
	DisConnect();
	DeleteCriticalSection(&m_cs);
}

BOOL COracle::Connect( HostParam host )
{
	memcpy(&m_hostParam, &host, sizeof(HostParam));
	return Connect(host.hostName,host.hostPort,host.hostDbName,host.hostUser,host.hostPsw);
}

BOOL COracle::Connect(orastring hostName, int hostPort, orastring hostDbName,orastring hostUser, orastring hostPsw)
{
	// 如已有连接,先断开在继续
	if (IsConnected())
		DisConnect();

	m_hostParam.hostName = hostName;
	m_hostParam.hostPort = hostPort;
	m_hostParam.hostDbName = hostDbName;
	m_hostParam.hostUser = hostUser;
	m_hostParam.hostPsw = hostPsw;

	orastring hostString;
	if (m_hostParam.hostName.length() <= 0)
		hostString = hostDbName;
	else{
		orastringstream oss;
		oss << m_hostParam.hostName << (":") << m_hostParam.hostPort <<("/") << m_hostParam.hostDbName;
		hostString = oss.str();
	}

	m_errMsg.clear();
	m_pCon = OCI_ConnectionCreate(hostString.c_str(),m_hostParam.hostUser.c_str(),(m_hostParam.hostPsw.c_str()),OCI_SESSION_DEFAULT);

	const mtext* err =  OCI_ErrorGetString(OCI_GetLastError());
	m_errMsg = err?err:"";

	return m_pCon != NULL;
}

BOOL COracle::DisConnect()
{
	if (IsConnected())
	{
		OCI_ConnectionFree(m_pCon);
		m_pCon=0;
	}
	return false;
}


void COracle::Attach(OCI_Connection* pCon)
{
//	ASSERT(pCon != NULL);
	m_pCon = pCon;
}

BOOL COracle::InitOCILib(void)
{
	return OCI_Initialize(NULL, NULL, OCI_ENV_THREADED|OCI_ENV_CONTEXT);
}

void COracle::FreeOCILib(void)
{
	OCI_Cleanup();
}


//=================================================CExecutor====
#ifdef ORACLE_EXEC_USE_LOG 
CLoging g_log;
#endif
OCI_Resultset* CDBExecutor::Query(orastring sql)
{
	if( !this->Exec(sql.c_str())){
		return 0;
	}
	return OCI_GetResultset(m_stat);
}
void print_error(OCI_Error* err){
	if(!err){
		printf("#####There is no OCI error .");
		return;
	}
	printf(
		"code  : ORA-%05i\n"
		"msg   : %s\n"
		"sql   : %s\n",
		OCI_ErrorGetOCICode(err), 
		OCI_ErrorGetString(err),
		OCI_GetSql(OCI_ErrorGetStatement(err))
		);
}
bool CDBExecutor::Exec(orastring sql)
{
	OCI_Error * err=0;
	m_stat = OCI_StatementCreate(m_ora->GetConPtr());
	boolean ok = OCI_SetFetchMode(m_stat, OCI_SFM_SCROLLABLE);
	if(!m_stat){
#ifdef ORACLE_EXEC_USE_LOG
		m_temp.Format(_T("OCI_StatementCreate() %s"),sql);
		g_log.DLog(g_str);
#endif
		err=OCI_GetLastError();
		print_error(err);
		return false;
	}
	if(!OCI_Prepare(m_stat,sql.c_str())){
#ifdef ORACLE_EXEC_USE_LOG
		m_temp.Format(_T("OCI_Prepare() %s"),sql);
		g_log.DLog(g_str);
#endif
		OCI_StatementFree(m_stat);
		m_stat = 0;
		err=OCI_GetLastError();
		print_error(err);
		return false;
	}
	if(!OCI_Execute(m_stat)){
#ifdef ORACLE_EXEC_USE_LOG
		m_temp.Format(_T("OCI_Execute() %s"),sql);
		g_log.DLog(g_str);
#endif
		err=OCI_GetLastError();
		print_error(err);
		OCI_StatementFree(m_stat);
		m_stat = 0;
		return false;
	}
	return true;
}
