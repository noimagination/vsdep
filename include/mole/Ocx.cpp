#include "stdafx.h"
#include "Ocx.h"


COcx::COcx(LPCTSTR ocxFile, LPCTSTR class_name)
{
	m_file = ocxFile;
	m_name = class_name;
}


COcx::~COcx()
{
}

bool COcx::IsRegistered()
{
	HKEY hk;
	TCHAR szPath[MAX_PATH] = { 0 };
	_stprintf_s(szPath, MAX_PATH, _T("%s\\clsid"), m_name);
	return RegOpenKeyEx(HKEY_CLASSES_ROOT, szPath, 0, KEY_READ, &hk) == ERROR_SUCCESS;
}
bool COcx::Register()
{
	HINSTANCE hOCX = LoadLibrary(m_file.c_str());
	if (hOCX){
		return false;
	}
	FARPROC lpDllEntry;
	lpDllEntry = GetProcAddress(hOCX, (LPCSTR)_T("DllRegisterServer"));
	if (lpDllEntry == 0){
		return false;
	}
	return TRUE==lpDllEntry();
}
bool COcx::Unregister()
{
	HINSTANCE hOCX = LoadLibrary(m_file.c_str());
	if (!hOCX){
		return false;
	}

	FARPROC lpDllEntry;
	lpDllEntry = GetProcAddress(hOCX, (LPCSTR)_T("DllUnregisterServer"));
	if (!lpDllEntry){
		return false;
	}
	return TRUE == (*lpDllEntry)();
}