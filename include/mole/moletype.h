#ifndef _MOLE_TYPE_H_
#define _MOLE_TYPE_H_

#include <tchar.h>
#include <vector>
#include <list>
#include <map>
#include <queue>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <time.h>
#include <windows.h>

using namespace std;

#define __IN
#define __OUT
#define __GLOBAL

#define MOLE_USE_WCHAR
#ifdef MOLE_USE_WCHAR
typedef wstring tstring;
typedef wofstream tofstream;
typedef wifstream tifstream;
typedef wostringstream tostringstream;
#define tcout wcout
typedef wchar_t tchar;
#else
typedef string tstring;
typedef ofstream tofstream;
typedef wifstream tifstream;
typedef ostringstream tostringstream;
#define tcout cout
typedef char tchar;
#endif

typedef tstring mstring;
typedef tchar mchar;


#endif // _MOLE_TYPE_H_