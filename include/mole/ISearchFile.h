#pragma once
#include <string>
using namespace std;
/**!
	文件搜索接口,不能直接使用
	@tip:继承此接口，实现 OnSearch()
*/
class ISearchFile
{
public:
	ISearchFile(){};
	~ISearchFile(void){};
public:
	/**! 搜索回调
	@id Search 的第二个参数
	@what Search 的第一个参数
	@wfd 搜索到的结果
	@return false 终止，true 继续查找
	 */
	virtual bool OnSearched(DWORD id,const char* what,WIN32_FIND_DATAA& wfd)=0;

	/**!开始搜索
		@what : 同 FindFirstFile
		@id	  : 在回调中体现，让你知道调用的哪个
		@return -1 没找到，1 用户终止，0 正常处理结束
	 */
	int Search(const char* what,DWORD id){
		WIN32_FIND_DATAA wfd;
		HANDLE hFind = 0;
		BOOL bFound = TRUE;
		int ret = 0;
		string src,path,type;
		src=what;
		string::size_type p= src.find_last_of("\\/");
		path = src.substr(0,p+1);
		type = src.substr(p);

		hFind = FindFirstFileA(what,&wfd);
		if(INVALID_HANDLE_VALUE == hFind){
			return -1;
		}

		while(bFound){
			if(!this->OnSearched(id,what,wfd)){
				ret = 1;
				break;
			}
			if(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY &&
				_stricmp(wfd.cFileName,".") != 0 &&
				_stricmp(wfd.cFileName,"..") != 0){
				this->Search((path+wfd.cFileName+type).c_str(),id);
			}
			bFound = FindNextFileA(hFind,&wfd);
		}

		FindClose(hFind);

		return ret;
	}
};

