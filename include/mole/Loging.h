#pragma once
#include <mole/moletype.h>

#define LOG_USE_WCHAR
#ifdef LOG_USE_WCHAR
typedef wchar_t LCHAR;
typedef wstring lstring;
#else
typedef char LCHAR;
typedef string lstring;
#endif

class CLoging
{
public:
	CLoging(void);
	~CLoging(void);
public:
	void SetAlive(bool val=true);
	void SetLogFile(const LCHAR* fullpath);
	void Log(const LCHAR* content,bool with_time=true);
	void DLog(const LCHAR* c,bool wt=true);
private:
	lstring m_logfile;
	bool m_bAlive;//是否在进程过程中一直打开
	FILE* m_fp;
};

