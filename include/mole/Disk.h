#pragma once

#include "moletype.h"
#include <wtypes.h>

#ifdef _UNICODE
#define tsystem _wsystem
#else
#define tsystem system
#endif
// 
typedef unsigned __int64 P_SIZE;

// 分区容量单位
typedef enum {
	PSU_K=1024,
	PSU_M=1024*1024,
	PSU_G=1024*1024*1024,
}PARTITION_SIZE_UNIT;

// 文件查找时回调
class IFileSearchCallback{
public:
	virtual int OnFound(bool isDir,LPCTSTR name,DWORD dwUser) =0;
};
/**
 * 分区类
 * 在构造类对象时传入参数，调用方自行检查合法性，非法参数则容量全为0
 */
class CPartition{
public:
	CPartition(const TCHAR* fromPath);
	~CPartition();
public:
	/** 清空某个目录下的空目录 
	  @param : root 目录
	**/
	static void ClearEmptyFolder(const TCHAR* root);
	/** 文件或目录是否存在
     **/
	static BOOL IsFileExists(const TCHAR * file);
	/** 创建目录,深度不限
	**/
	static bool CreateDir(LPCTSTR dir);
	/** 文件或目录是否存在(模糊匹配，以 file 开头)
	 */
	static bool IsFileExistsEx(LPCTSTR file);
	/** 删除目录*/
	static BOOL DeleteDirectory(LPCTSTR dir);
	/**  查找文件 
	 @cond:查找条件，形如 c:\*.*
	 @cb:查找回调函数，每找到一个文件就回调
	 @dwUser: 用户数据。在回调中含这个值
	 @return:
	 **/
	static int Search(LPCTSTR cond,IFileSearchCallback* cb,DWORD dwUser=0);
	// 获取软件运行名
	static tstring GetMe();
	//获取软件运行路劲
	static tstring GetThisPath();
	/** 获得分区总大小
	 @param : psu 容量计算单位
	 #return : 指定单位的容量大小
	 **/
	P_SIZE GetTotal(PARTITION_SIZE_UNIT psu=PSU_G);
	/** 获得分区 空闲容量
	 @param : psu 容量计算单位
	 #return : 指定单位的容量大小
	 **/
	P_SIZE GetFree(PARTITION_SIZE_UNIT psu=PSU_G);
	/** 获得分区名称 eg. C: D:
	**/
	tstring GetPartitionName();
	// 分区容量信息统计，结果保存在成员 m_maxSize m_freeSize 中
	void StatisticPartition();
private:
	
	P_SIZE m_maxSize,m_freeSize;	// 分区容量，可用容量
	tstring m_partition;					// 分区名 c:
	static tstring ms_current_dir;  //当前目录
	static tstring ms_me;
};

// 磁盘类
class CDisk
{
public:
	CDisk(void);
	~CDisk(void);
	vector<CPartition*> m_partitions;
};

