#ifndef SAT_HTTPCLIENT_20141123
#define SAT_HTTPCLIENT_20141123

#include <windows.h>
#include <WinInet.h>

#define LEN_HOSTNAME 128
#define LEN_URLPATH 256

class CHttpClient{
public:
	CHttpClient();
	~CHttpClient();

public:
	static bool init();
	static bool destroy();

	/** 下载某个 URL 的文件到本地，GET 方式
	 @url : 下载地址
	 @save_to : 保存的本地文件
	 */
	bool download_file(LPCTSTR url,LPCTSTR save_to);
	/** 下载某个 URL 的文件到内存，GET 方式
	 @url : 下载地址
	 @save_to : 保存到的内存地址
	 */
	bool download_mem(LPCTSTR url,char* buffer);
	/*** POST 数据到某个URL 地址 
	 */
	bool post(LPCTSTR url,char* data);
private:
	void reset();
	bool connect(LPCTSTR url);
	bool disconnect();
private:
	static HINTERNET m_hInternet;
	TCHAR m_hostName[LEN_HOSTNAME];
	TCHAR m_urlPath[LEN_URLPATH];
	HINTERNET m_hConnection;
	URL_COMPONENTS m_url;
};

#endif
