

#include "XmlSchema.h"

typedef slim::XmlDocument xDoc;
typedef slim::XmlNode* xNode;
typedef slim::XmlAttribute* xAttr;
typedef slim::NodeIterator iterNode;

#define NULL_RETURN_FALSE(ptr) if(!(ptr)){return false;}
#define NULL_RETURN(ptr,ret) if(!(ptr)){return ret;}

#define GET_VAL_CHARSTR(node,var,def) if(node){Strcpy(var,node->getString());}else{var=def;}
#define GET_VAL_STR(node,var,def) if(node){var=node->getString();}else{var=def;}
#define GET_VAL_INT(node,var,def) if(node){var=node->getInt();}else{var=def;}
#define GET_VAL_FLOAT(node,var,def) if(node){var = node->getFloat();}else{var=def;}
#define GET_VAL_ATTR_STR(attr,var,def) if(attr){var=attr->getString();}else{var=def;}
#define GET_VAL_ATTR_INT(attr,var,def) if(attr){var =attr->getString();}else{var=def;}

#define SET_VAL_INT(parent,node,node_name,node_value) node = parent->addChild(node_name);\
	if (node != nullptr)\
		node->setInt(node_value);
#define SET_VAL_STR(parent,node,node_name,node_value) node = parent->addChild(node_name);\
	if (node != nullptr)\
		node->setString(node_value);