#ifndef ITAS_CONVERT_H
#define ITAS_CONVERT_H
#include <string>
using namespace std;

typedef LPCTSTR ITASSTR;
typedef wstring ITASSTRING;
/*方向字符串--方向代码*/
inline ITASSTR dir_2_dirCode(ITASSTR dir) {
	ITASSTRING s = dir;
	ITASSTRING::size_type pos = s.find(_T("东"));
	ITASSTRING::size_type pos2 = ITASSTRING::npos;

	if (pos != ITASSTRING::npos) {
		pos2 = s.find(_T("西"));
		if (pos2 == ITASSTRING::npos) {
			return _T("37");
		}
		if (pos > pos2) {
			return _T("73");
		}
		else {
			return _T("37");
		}
	}
	pos = s.find(_T("南"));
	if (pos != ITASSTRING::npos) {
		pos2 = s.find(_T("北"));
		if (pos2 == ITASSTRING::npos) {
			return _T("15");
		}
		if (pos > pos2) {
			return _T("15");
		}
		else {
			return _T("51");
		}
	}
	return _T("15");	//default
}
//车牌颜色字符串--车牌颜色代码
inline ITASSTR plateClr_2_plateClrCode(ITASSTR pclr) {
	ITASSTRING color = pclr;
	if (color.find(_T("蓝")) != ITASSTRING::npos) {
		return _T("2");
	}
	else if (color.find(_T("黄")) != ITASSTRING::npos) {
		return _T("1");
	}
	else if (color.find(_T("白")) != ITASSTRING::npos) {
		return _T("6");
	}
	else if (color.find(_T("黑")) != ITASSTRING::npos) {
		return _T("3");
	}
	else {
		return _T("9");
	}
}
#endif